package com.example.gradle;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import java.util.Optional;

@RestController
public class HelloWorldController {

    @GetMapping(value = {"/hello", "hello/{name}"})
    public String hello(@PathVariable Optional<String> name) {
        return name.isPresent()
                ? "Hello " + name.get() + "!"
                : "Hello World!";
    }
}
