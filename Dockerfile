FROM openjdk:8-jdk-alpine AS builder
WORKDIR application
COPY build/libs/*.jar application.jar
RUN java -Djarmode=layertools -jar application.jar extract

FROM openjdk:8-jdk-alpine
WORKDIR application
COPY --from=builder application/dependencies .
COPY --from=builder application/spring-boot-loader .
COPY --from=builder application/snapshot-dependencies .
COPY --from=builder application/application .
ENTRYPOINT ["java", "org.springframework.boot.loader.JarLauncher"]
